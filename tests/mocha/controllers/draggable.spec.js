describe("Draggable", function() {

  // create the demo element we need to test
  var draggableElement = $("<div class='js-draggable'></div")

  before(function (done) {
    // we need to insert the button before the tests so we can test it
    $('body').append(draggableElement)
    done()
  })

  after(function (done) {
    // this is only for the view of the tests, so it stays at top once it finished
    $(draggableElement).remove()
    done()
  })

  describe('translateElement', function() {
    it('Should move the element to a new position', function () {
      var target = $(draggableElement)[0]
      var x = 40
      var y = 20
      // call the method to change the original position of the element
      translateElement(target, x, y)
      expect($(target)).to.have.attr('style', 'transform: translate(' + x + 'px, ' + y + 'px);')
      expect($(target)).to.have.data('x', x)
      expect($(target)).to.have.data('y', y)
    })
  })
});
