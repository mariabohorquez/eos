/* ==========================================================================
  Menu unit testing
  ========================================================================== */
describe('Menu', () => {
  const menuWrap = $('<aside class="js-main-menu"><div class="mm-navigation-container"><header><a class="hide-collapsed" href="/"></a><i class="material-icons js-sidebar-toggle">menu</i></header><ul class="no-list-style"><li data-placement="right" title="Dashboard" showtooltip="hide"><a class="active" href="/dashboard"></ul></div></aside>')

  describe('Collapse status based on windows size', () => {
    before((done) => {
      $('body').prepend(menuWrap)

      $(menuWrap).css({"width": "250px"})
      done()
    })

    afterEach((done) => {
      $('.js-main-menu').removeClass('collapsed-sidebar')
      done()
    })

    /* Remove menu after testing it */
    after((done) => {
      $(menuWrap).remove()
      done()
    })


    it('Should detect small screens', () => {
      window.innerWidth = 800;
      expect(isSmallScreen()).to.be.true
    })

    it('Should detect big screens', () => {
      window.innerWidth = 1380;
      expect(isSmallScreen()).to.be.false
    })

    it('Should automatically collapse the menu in small screen', () => {
      window.innerWidth = 800

      collapseSidebarOnSmallScreen ()
      expect(menuWrap).to.have.class('collapsed-sidebar')
      expect(collapseSidebarOnSmallScreen()).to.be.true
    })


    it('Should automatically open up the menu in big screen', () => {
      window.innerWidth = 1380
      autoToggleSidebar()

      expect(menuWrap).not.to.have.class('collapsed-sidebar')
    })

    it('Should automatically hide in small screen', () => {
      window.innerWidth = 800
      autoToggleSidebar()

      expect(menuWrap).to.have.class('collapsed-sidebar')
    })

  })
})
