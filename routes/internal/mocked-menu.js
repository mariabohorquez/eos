const express = require('express')
const router = express.Router()

/* GET home page. */
router.get('/', (req, res, next) => {
  if (process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === 'production') {
    res.redirect('/dashboard')
  } else {
    res.render('internal/mocked-menu', { title: 'Mocked Scalable Menu', path: req.originalUrl })
  }
})

module.exports = router
